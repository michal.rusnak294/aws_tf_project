resource "aws_instance" "example" {
  ami           = "ami-0b93ce03dcbcb10f6"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.main.id]
}
